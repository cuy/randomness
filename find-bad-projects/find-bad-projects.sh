#!/bin/bash

export CORRCET_BILLING=xxxxxx-xxxxxx-xxxxxx
export ORG_ID=`gcloud organizations list --format="value(ID)"`
gcloud alpha scc assets list $ORG_ID --format=json --filter="securityCenterProperties.resourceType=\"google.cloud.resourcemanager.Project\"" --format="value(asset.resourceProperties.projectId)" > org_projects.txt
gcloud beta billing projects list --billing-account $CORRECT_BILLING --format="value(project_id)" > billed_projects.txt
echo Projects in Org on incorrect billing account
printf "%-40s %-40s\n" "Project" "Owners"

for p in `grep -vf billed_projects.txt org_projects.txt`; do \
  if hash jq 2> /dev/null; then \
    owners=`gcloud projects get-iam-policy $p --filter="bindings.role=roles/owner" --format=json | jq -r '.[] | .bindings[] as $parent | select($parent.role=="roles/owner") |$parent.members | @csv'`
  else owners="jq not installed"
  fi
 printf "%-40s %-40s\n" "$p" "$owners"
done


