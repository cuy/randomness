import base64
from google.cloud import bigquery
from google.cloud import storage
from datetime import date
from datetime import timedelta
from datetime import datetime
import logging
import os 
import json
from googleapiclient.discovery import build

def export_billing(event, context):
    BILLING_TEMP_TABLE="billing_export_temp"

    try:
        BQ_PROJECT_ID=os.environ['BQ_PROJECT_ID']
        BQ_DATASET=os.environ['BQ_DATASET']
        BQ_TABLE=os.environ['BQ_TABLE']
        BILLING_BUCKET_NAME=os.environ['BILLING_BUCKET_NAME']
    except KeyError as k:
        logging.error("environment variable: {} is missing".format(str(k)))
        return
    
    yesterday_date = date.today() - timedelta(days = 1)
    filename = "billing_export-" + str(yesterday_date)

    destination_uri = "gs://{}/{}".format(BILLING_BUCKET_NAME, filename + "-*.json") # need wildcard as export shards on > 1 GB
    client = bigquery.Client()

    # query = """
    # create or replace table {BQ_PROJECT_ID}.{BQ_DATASET}.{TEMP_TABLE} as (
    #     SELECT billing_account_id, service.id as service_id, service.description as service_description, 
    #       sku.id as sku_id, sku.description as sku_description, usage_start_time,
    #       usage_end_time, project.id AS project_id, project.name AS project_name,
    #       project.ancestry_numbers AS project_ancestry_numbers,
    #       TO_JSON_STRING(project.labels) AS project_labels, TO_JSON_STRING(labels) AS labels,
    #       TO_JSON_STRING(system_labels) AS system_labels, 
    #       location.location as location,
    #       location.country as location_country,
    #       location.region as location_region,
    #       location.zone as location_zone, 
    #       export_time,
    #       cost, currency, currency_conversion_rate, 
    #       usage.amount as usage_amount,
    #       usage.unit as usage_unit,
    #       usage.amount_in_pricing_units as usage_amount_in_pricing_units,
    #       usage.pricing_unit as usage_pricing_unit,
    #       TO_JSON_STRING(credits) as credits,
    #       invoice.month as invoice_month, cost_type
    #     FROM `{BQ_PROJECT_ID}.{BQ_DATASET}.{BQ_TABLE}`
    #     WHERE DATE(_PARTITIONTIME) BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY) AND DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY))
    # """.format(BQ_PROJECT_ID=BQ_PROJECT_ID,BQ_DATASET=BQ_DATASET,BQ_TABLE=BQ_TABLE,TEMP_TABLE=BILLING_TEMP_TABLE)


    query = """
    create or replace table {BQ_PROJECT_ID}.{BQ_DATASET}.{TEMP_TABLE} as (
        SELECT *
        FROM `{BQ_PROJECT_ID}.{BQ_DATASET}.{BQ_TABLE}`
        WHERE DATE(_PARTITIONTIME) BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY) AND DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY))
    """.format(BQ_PROJECT_ID=BQ_PROJECT_ID,BQ_DATASET=BQ_DATASET,BQ_TABLE=BQ_TABLE,TEMP_TABLE=BILLING_TEMP_TABLE)

    query_job = client.query(query)
    results = query_job.result() 

    dataset_ref = client.dataset(BQ_DATASET,BQ_PROJECT_ID)
    table_ref = dataset_ref.table(BILLING_TEMP_TABLE)
    print("Running extract job")

    job_config = bigquery.job.ExtractJobConfig()
    job_config.destination_format = (bigquery.DestinationFormat.NEWLINE_DELIMITED_JSON)
    # job_config.destination_format = (bigquery.DestinationFormat.CSV)

    extract_job = client.extract_table(
        table_ref,
        destination_uri,
        location="US",
        job_config=job_config,
    )  # Extract into < 1 GB objects
    extract_job.result()  # Waits for job to complete.
    
    # delete table now that we're done
    client.delete_table(table_ref)

    # Connect to GCS and compose all the blobs into a single object then clean up temp objects
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(BILLING_BUCKET_NAME)
    blobs = list(storage_client.list_blobs(
        BILLING_BUCKET_NAME, prefix=filename+"-", delimiter='/'
    ))

    final_name = filename + '.json'
    
    if len(blobs) > 1: # Check for compose or rename required
        blob = bucket.blob(final_name)
        blob.compose(blobs)
        for blob in blobs:
            blob.delete()
    else:
        new_blob = bucket.rename_blob(blobs.pop(),final_name)

    print("Creating dataflow job to push to pub/sub")
    send_to_pubsub("gs://" + BILLING_BUCKET_NAME + "/" + final_name)
 
def send_to_pubsub(searchpattern):
    try:
        outputTopic = os.environ['TOPIC']
        project=str.split(outputTopic,'/')[1]
    except KeyError as k:
        logging.error("environment variable: {} is missing".format(str(k)))
        return

    dataflow = build('dataflow', 'v1b3')
    request = dataflow.projects().templates().launch(
        projectId=project,
        gcsPath="gs://dataflow-templates/latest/GCS_Text_to_Cloud_PubSub",
        body={
            'jobName': 'billing-export-' + date.strftime(datetime.now(),"%Y%m%d-%H%M"),
            'parameters': {
                "inputFilePattern": searchpattern,
                "outputTopic": outputTopic
            },
            "environment": { "zone": "us-central1-f" }
        }
    )
    response = request.execute()

# Used for testing locally
if __name__ == '__main__':
    event={'data': base64.b64encode("abc".encode('utf-8')) }
    context=""
    export_billing(event,context)