import base64
from google.cloud import bigquery
from google.cloud import storage
from datetime import date
from datetime import timedelta
from datetime import datetime
import logging
import os 
import json
from googleapiclient.discovery import build
import sys
import re

BQ_PROJECT_ID="<PROJECT>"
BQ_DATASET="<DATASET>"
BQ_TABLE="<BILLING_TABLE>"
BILLING_BUCKET_NAME="<BILLING_BUCKET>"
TOPIC="<PUBSUB_TOPIC>"

def export_billing(backfill_date):
    BILLING_TEMP_TABLE="billing_export_temp"
   
    run_date = datetime.strftime(datetime.strptime(backfill_date,"%Y-%m-%d") + timedelta(days = 1),"%Y-%m-%d")
    filename = "billing_export-" + backfill_date + "-manual"

    destination_uri = "gs://{}/{}".format(BILLING_BUCKET_NAME, filename + "-*.json") # need wildcard as export shards on > 1 GB
    client = bigquery.Client()

    query = """
       create or replace table {BQ_PROJECT_ID}.{BQ_DATASET}.{TEMP_TABLE} as (
           WITH
  t1 AS (
  SELECT COUNT(*) AS billing_items FROM `{BQ_PROJECT_ID}.{BQ_DATASET}.{BQ_TABLE}`
  WHERE
    DATE(_PARTITIONTIME) BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY)
    AND DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY) )
SELECT
  g._PARTITIONDATE AS billing_partition_date,
  t1.billing_items, g.*
FROM `{BQ_PROJECT_ID}.{BQ_DATASET}.{BQ_TABLE}` g
JOIN t1 ON 1=1
WHERE
  DATE(_PARTITIONTIME) BETWEEN DATE_SUB(\"{backfill_date}\", INTERVAL 1 DAY)
  AND DATE_SUB(\"{backfill_date}\", INTERVAL 1 DAY)
  )
    """.format(BQ_PROJECT_ID=BQ_PROJECT_ID,BQ_DATASET=BQ_DATASET,BQ_TABLE=BQ_TABLE,TEMP_TABLE=BILLING_TEMP_TABLE,backfill_date=run_date)

    query_job = client.query(query)
    results = query_job.result() 

    dataset_ref = client.dataset(BQ_DATASET,BQ_PROJECT_ID)
    table_ref = dataset_ref.table(BILLING_TEMP_TABLE)
    print("Running extract job")

    job_config = bigquery.job.ExtractJobConfig()
    job_config.destination_format = (bigquery.DestinationFormat.NEWLINE_DELIMITED_JSON)
    # job_config.destination_format = (bigquery.DestinationFormat.CSV)

    extract_job = client.extract_table(
        table_ref,
        destination_uri,
        location="US",
        job_config=job_config,
    )  # Extract into < 1 GB objects
    extract_job.result()  # Waits for job to complete.
    
    # delete table now that we're done
    client.delete_table(table_ref)

    # Connect to GCS and compose all the blobs into a single object then clean up temp objects
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(BILLING_BUCKET_NAME)
    blobs = list(storage_client.list_blobs(
        BILLING_BUCKET_NAME, prefix=filename+"-", delimiter='/'
    ))

    final_name = filename + '.json'
    
    if len(blobs) > 1: # Check for compose or rename required
        blob = bucket.blob(final_name)
        blob.compose(blobs)
        for blob in blobs:
            blob.delete()
    else:
        new_blob = bucket.rename_blob(blobs.pop(),final_name)

    print("Wrote: {}".format(final_name))
    print("Creating dataflow job to push to pub/sub")
    send_to_pubsub("gs://" + BILLING_BUCKET_NAME + "/" + final_name)
 
def send_to_pubsub(searchpattern):
    try:
        outputTopic = TOPIC
        project=str.split(outputTopic,'/')[1]
    except KeyError as k:
        logging.error("environment variable: {} is missing".format(str(k)))
        return

    dataflow = build('dataflow', 'v1b3')
    request = dataflow.projects().templates().launch(
        projectId=project,
        gcsPath="gs://dataflow-templates/latest/GCS_Text_to_Cloud_PubSub",
        body={
            'jobName': 'billing-export-' + date.strftime(datetime.now(),"%Y%m%d-%H%M"),
            'parameters': {
                "inputFilePattern": searchpattern,
                "outputTopic": outputTopic
            },
            "environment": { "zone": "us-central1-f" }
        }
    )
    response = request.execute()

if __name__ == '__main__':
    rundate=sys.argv[-1]
    if (re.match("^\d{4}-\d{2}-\d{2}$",rundate)):
        print(f"Performing manual run for file {rundate}")
        export_billing(rundate)
    else:
        print("Run python3 manual_backfill.py YYYY-MM-DD")
        sys.exit(0)
        