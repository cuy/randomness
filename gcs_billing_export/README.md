## Configuraiton

### Create pub/sub topic to kick off the Cloud Function
```shell
PUBSUB_TOPIC=billing_export
gcloud pubsub topics create $PUBSUB_TOPIC
```

### Create the bucket to assign store the billing export
```shell
BILLING_BUCKET_NAME={bucket name}
gsutil mb gs://$BILLING_BUCKET_NAME
```

### Ensure the service account for the cloud function has permissions
Service account: <project>@appspot.gserviceaccount.com
Need permissions:
- BQ User on BQ_dataset
- Permission to Billing_bucket


### Deploy Cloud Function
```shell
BQ_PROJECT_ID={project id with billing bq export}
BQ_DATASET={billing data set name}
BQ_TABLE={table with billing data}
TOPIC={topic that dataflow is reading and sending to splunk}

gcloud functions deploy gcs_billing_export --entry-point export_billing --runtime python37 --trigger-topic $PUBSUB_TOPIC --timeout=540 \
--set-env-vars BQ_PROJECT_ID="$BQ_PROJECT_ID",BQ_DATASET="$BQ_DATASET",BQ_TABLE="$BQ_TABLE",BILLING_BUCKET_NAME="$BILLING_BUCKET_NAME",TOPIC="$TOPIC"
```

### Create a scheduler to run on a daily basis
```shell
gcloud scheduler jobs create pubsub daily_billing_export --schedule "0 21 * * *" --topic $PUBSUB_TOPIC --message-body " "
```

## Optional

### Run on a weekly basis instead
```shell
gcloud scheduler jobs create pubsub daily_billing_export --schedule "0 21 1 * *" --topic $PUBSUB_TOPIC --message-body "7"
```

### Backfill 30 days
```shell
gcloud pubsub topics publish $PUBSUB_TOPIC --message '30'
```

### Manually run
```shell
gcloud pubsub topics publish $PUBSUB_TOPIC --message ' '
```