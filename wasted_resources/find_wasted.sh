#!/bin/bash

PROJECTS=`gcloud projects list --format="value(projectId)"`
TOTAL=0
for PROJECT in $PROJECTS; do \
    COMPUTE=`gcloud services list --project=$PROJECT --filter="NAME=compute.googleapis.com" --format="value(NAME)" 2> /dev/null`
    if [[ $? == 0 ]]; then \
        if [[ "$COMPUTE" != "" ]]; then \
            REC=`gcloud services list --project=$PROJECT --filter="NAME=recommender.googleapis.com" --format="value(NAME)"`
            if [[ $REC == "" ]]; then \
                gcloud services enable recommender.googleapis.com --project=$PROJECT -q 2> /dev/null
            fi
            if [[ $? == 0 ]]; then \
                ZONES=`gcloud compute instances list --project=splunk-qwiklabs --filter="STATUS=RUNNING" --format="value(ZONE)" | sort | uniq`
                for ZONE in $ZONES; do \

                    val=`gcloud recommender recommendations list --project=$PROJECT --recommender=google.compute.instance.MachineTypeRecommender --location=$ZONE --filter="PRIMARY_IMPACT_CATEGORY=COST" --format="csv[no-heading](content.operationGroups.operations[0].resource.segment(8),description,primaryImpact.costProjection.cost.units)"  | sed -e "s/'],Save cost by changing machine type from /\,/g" | sed -e 's/\.//g'`
                    node=`echo $val | cut -d, -f1`
                    desc=`echo $val | cut -d, -f2`
                    savings=`echo $val | cut -d, -f3`
                    savings=$((-1 * savings))
                    TOTAL=$((TOTAL + $savings))
                    if [ "$node" != "" ]; then \
                        echo "$PROJECT : Resize $node from $desc for a total monthly savings of $ $savings"
                    fi
                done
            else
                echo "$PROJECT : Unable to connect"
            fi  
        fi
    else   
        echo "$PROJECT : No permission on project"
    fi
done

echo "Total potential savings: $ $TOTAL"
