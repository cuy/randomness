import base64
import requests
import json
import logging
import urllib3
import urllib.request
import ssl
from google.cloud import bigquery
import datetime
import sys

HEC_URL="https://[[HEC IP ADDRESS]]:8088/services/collector/event"
HEC_TOKEN='[[HEC TOKEN]]'
HTTP_TIMEOUT = 10
IGNORE_SSL_ERROR = True

BQ_PROJECT_ID="[[PROJECT ID OF ACCOUNT WITH BILLING EXPORT DATASET]]"
BQ_DATASET="[[DATASET_NAME.TABLE_NAME OF BQ EXPORT]]"

def send_to_splunk(event_obj):
    data=json.dumps(event_obj).encode('utf-8')
    r = urllib.request.Request(HEC_URL, data, authHeader)
    response = urllib.request.urlopen(r,context=ssl.SSLContext(), timeout=HTTP_TIMEOUT)
    return response.code

# This is to handle BQ returning a datatime function, not actual string we can put into Splunk
def dconvert(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()

def process_bq_export(minutes_ago):
    time_diff =  datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(minutes=minutes_ago)
    print("Querying data from BigQuery for last {} minutes".format(str(minutes_ago)))
    client = bigquery.Client(project=BQ_PROJECT_ID)
    table = client.get_table(BQ_DATASET)
    query_job = client.query("select * from `"+BQ_PROJECT_ID+"."+BQ_DATASET+"` where export_time > '"+str(time_diff)+"'")
    records = [dict(row) for row in query_job]
    status={}
    c=0
    total_records = len(records)
    status['200']=0
    print("Records to send to Splunk: {}".format(total_records))
    for row in records:
        json_obj = json.dumps(row, default = dconvert)
        event_obj = {
            "source":"gcp",
            "sourcetype":"gcp_billing_export",
            "event": json_obj
        }
        # Need to build some logic to batch these together but not so big it's an issue
        rc=send_to_splunk(event_obj)
        try:
            status[str(rc)]+=1
        except:
            status[str(rc)]=1
        c+=1
        if (c % 100 == 0):
            print("Processs: {}/{}".format(c,total_records))
        else:
            print(".",end='', flush=True)
        
    for rc in status:
        print("\nRecords sent successfully: {}\nErrors: {}".format(status['200'],total_records-status['200']))

    if total_records-status['200']>0:
        return 1
    else:
        return 0

if __name__ == "__main__":
    # Ignore SSL warnings (needed for test)
    if IGNORE_SSL_ERROR:
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE
        urllib3.disable_warnings()

    authHeader = {'Authorization': 'Splunk ' + HEC_TOKEN}
    if(len(sys.argv)==2):
        exit(process_bq_export(int(sys.argv[1])))
    else:
        print("Run ./export_billing_to_splunk.py <minutes to process>")
        exit(1)
    