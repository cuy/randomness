# README.md
This repo is a colleciton of scripts/deployments used for demo purposes only.  Code here is provided as is.

## Current projects
1. splunk-dataflow - Terraform deployment for the Splunk Dataflow Template on GCP. Additionally includes some utilities for manual deployment and deadletter queue message handling.
2. splunk_billing_export - Sample export utility to migrate GCP Billing BQ Export to Splunk.  This allows the use of additional metadata not found in the CSV export.
3. terraform-network-example - A sample Terraform network deployment that has customer project connected to a master proejct via a clustered NAT gateway.
4. iam_finder - A simple script to export all permissions in a gcp org and then search for a specific
   user/group/seriveaccount/domain and show at what level permission is given.  It does not show effective permissions
   at a particular level - for that use the console.
