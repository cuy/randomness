This function will spin up a dataflow -> pub/sub batch job to stream a file from a bucket into 
pub/sub (ideally for sending to Splunk via the dataflow->splunk streaming template).  
It takes about 3-5 minutes after object is created before it's fully streamed into pub/sub.  
This is designed to be used on buckets with very periodic objects added (hourly assets export, 
daily billing, etc..).  It is not recommended to use this function on highly active buckets as 
it can spin up a ton of dataflow jobs.

Note - this function will always submit the dataflow jobs in the project that houses the pub/sub 
topic - not the one the function is in.

![](https://gitlab.com/cuy/randomness/-/raw/master/gcs_dataflow_upload/function_architecture.png)


## Configuraiton
```shell
gcloud services enable dataflow.googleapis.com
```

### Ensure the service account for the cloud function has permissions
Service account: <project>@appspot.gserviceaccount.com
Need permissions:
- Read on bucket being monitored
- Permission to create a dataflow job


### Deploy Cloud Function
```shell
BUCKET={bucket to be monitored}
TOPIC={topic that dataflow is reading and sending to splunk - format projects/xxxxxx/topics/xxxxxxx}

gcloud functions deploy gcs_dataflow_upload --runtime python37 --trigger-resource $BUCKET --timeout=540 \
--set-env-vars TOPIC="$TOPIC" --trigger-event google.storage.object.finalize
```
