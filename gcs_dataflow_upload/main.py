import os
import time
import base64
import logging
from googleapiclient.discovery import build
from datetime import date
from datetime import datetime

def gcs_dataflow_upload(data, context):
    try:
        filename = "gs://" + data['bucket'] + "/" + data['name']
        print("Creating pipefile for {}".format(filename))
        send_to_pubsub(filename)
    except:
        print("Unable to determine object name or create dataflow job")
        return
    

def send_to_pubsub(searchpattern):
    try:
        outputTopic = os.environ['TOPIC']
        project=str.split(outputTopic,'/')[1]
    except KeyError as k:
        logging.error("environment variable: {} is missing".format(str(k)))
        return
    dataflow = build('dataflow', 'v1b3',cache_discovery=False)
    request = dataflow.projects().templates().launch(
        projectId=project,
        gcsPath="gs://dataflow-templates/latest/GCS_Text_to_Cloud_PubSub",
        body={
            'jobName': 'gcs-dataflow-export-' + date.strftime(datetime.now(),"%Y%m%d-%H%M"),
            'parameters': {
                "inputFilePattern": searchpattern + '*',
                "outputTopic": outputTopic
            },
            "environment": { "zone": "us-central1-f" }
        }
    )
    print("Deploying Dataflow pipeline")
    response = request.execute()

# Used for testing locally
if __name__ == '__main__':
    print("Going manual")
    event={'data': base64.b64encode("abc".encode('utf-8')) }
    event={'name':'test-file','bucket':'test-bucket-name'}
    context=""
    gcs_dataflow_upload(event,context)

