import base64
import logging
import os 
import json
import time
from google.cloud import resource_manager
from google.cloud import recommender
from google.cloud import logging
from googleapiclient import discovery
from google.cloud import asset_v1
import google.auth

LOG="GCE_Recommendations"

logger = logging.Client().logger(LOG)
timestamp = time.time()

def check_services(project):
    retval=False
    credentials, project_id = google.auth.default()
    service = discovery.build('serviceusage', 'v1', credentials=credentials, cache_discovery=False)
    request = service.services().get(name='projects/'+project+'/services/compute.googleapis.com')
    response = request.execute()
    if(response['state'] == "ENABLED"):
        request = service.services().get(name='projects/'+project+'/services/recommender.googleapis.com')
        response = request.execute()
        if(response['state'] != "ENABLED"):
            request = service.services().enable(name='projects/'+project+'/services/recommender.googleapis.com')
            response = request.execute()
        retval=True
    return retval


def get_recommendations(project,zone,rec_id):
    client = recommender.RecommenderClient()
    name = client.recommender_path(project, zone, rec_id)
    for recs in client.list_recommendations(name):
        if recs.state_info.state == 1:
            resource = recs.content.operation_groups[0].operations[0].resource
            item = "Unknown"
            resource_type = recs.content.operation_groups[0].operations[0].resource_type
            if resource_type == "compute.googleapis.com/Instance":
                item = resource.split('/')[8]
            if resource_type == "compute.googleapis.com/Snapshot" or resource_type == "compute.googleapis.com/Disk":
                item = "Disk"
            description = recs.description
            msg = {
                'insert_timestamp': timestamp,
                'project': project,
                'last_refresh': recs.last_refresh_time.seconds,
                'item': item,
                'zone': zone,
                'description': description,
                'savings': -1 * recs.primary_impact.cost_projection.cost.units
            }
            #print(msg)
            logger.log_struct(msg)


def get_projects(organization):
    projects={}
    scope='organizations/'+str(organization)
    client = asset_v1.AssetServiceClient()
    # TODO - add to 
    for inst in client.search_all_resources(scope,asset_types=["compute.googleapis.com/Instance"]):
        project = inst.name.split('/')[4]
        if project not in projects:
            projects[project]=[]
        if inst.location not in projects[project]:
            projects[project].append(inst.location)
    return projects

def savings_finder(event, context):
    print("Intilizating")
    ORG_ID = os.environ['ORG_ID']
    print("Getting project list")
    projects = get_projects(ORG_ID)
    for project in projects:
        print(f"Checking project {project}")
        if check_services(project):
            for zone in projects[project]:
                print(f" Getting recommendations in zone {zone}")
                get_recommendations(project,zone,'google.compute.instance.MachineTypeRecommender')
                get_recommendations(project,zone,'google.compute.instance.IdleResourceRecommender')
                get_recommendations(project,zone,'google.compute.disk.IdleResourceRecommender')
    





# Used for testing locally
if __name__ == '__main__':
    event={'data': base64.b64encode("abc".encode('utf-8')) }
    context=""
    savings_finder(event,context)