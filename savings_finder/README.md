
## Overview
The goal of this is dump all the VM resizing recommendations into a log entry that can further be forwarded to Splunk for use in a comprehensive view of potential savings across the entire organizations and potential waste. 

![splunk dashboard](splunk_dashboard.png)

## Configuraiton

### Service account
You'll need a service account with the following permissions at the org level
- Compute Viewer
- Logs Writer
- Viewer
- Service Account Admin
- Compute Recommender Viewer
- Service Usage Viewer

```shell
SERVICE_ACCOUNT=<accounts created above>
```
*(we'll use this variable in the function deployment)*

### Create pub/sub topic to kick off the Cloud Function
```shell
PUBSUB_TOPIC=savings_finder
gcloud pubsub topics create $PUBSUB_TOPIC
```

### Create the bucket to assign store the billing export
```shell
BILLING_BUCKET_NAME={bucket name}
gsutil mb gs://$BILLING_BUCKET_NAME
```

### Ensure the service account for the cloud function has permissions
Service account: <project>@appspot.gserviceaccount.com
Need permissions:
- Services admin (need to enable recommender service in each project)
- Recommender viewer


### Deploy Cloud Function
```shell
ORG_ID=`gcloud organizations list --format="value(ID)"`

gcloud functions deploy savings_finder --entry-point savings_finder --runtime python37 --trigger-topic $PUBSUB_TOPIC --timeout=540 \
--set-env-vars ORG_ID="$ORG_ID" --service-account $SERVICE_ACCOUNT
```

### Create a scheduler to run on a daily basis
```shell
gcloud scheduler jobs create pubsub savings_finder --schedule "0 21 * * *" --topic $PUBSUB_TOPIC --message-body " "
```

### Manually run
```shell
gcloud pubsub topics publish $PUBSUB_TOPIC --message ' '
```