#!/usr/bin/env python3

import json
import sys
import subprocess
import re
import argparse


def parse_args():
    parser = argparse.ArgumentParser(
        description='Search a GCP Org for permissions for a user and where they are granted',
        epilog="It's recommended to send the output of this is jq for better viewing")
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--org", help="Org ID to query - you must have gcloud configured with an account that has org owner permissions.  If you export this to a file it's easier to perform multiple searchs",)
    group.add_argument("--input",help="Specify file to read instead of running the full import of data (use --org to create this file)")
    parser.add_argument("--find",help="Search for a specific permissions - regular expressions allowed")
    args = parser.parse_args()
    return args



def run_cmd(cmd):
    cmd_array=cmd.split(" ")
    out = subprocess.Popen(cmd_array, 
           stdout=subprocess.PIPE, 
           stderr=subprocess.STDOUT)
    stdout,stderr = out.communicate()
    return stdout.decode('utf-8')

def clean_record(record):
    for val in ("Projects","SubFolders","Permissions"):
        try:
            if len(record[val])==0:
                record.pop(val)
        except:
            pass
    return record

def read_folder(id):
    id_type="--folder="
    retval = []
    if str(id) == str(ORG):
        id_type="--organization="
    output = run_cmd("gcloud resource-manager folders list " + id_type + str(id) + " --format=json")
    
    for d in json.loads(output):
        try:
            fid = d["name"].split("/")[1]
            name = d["displayName"]
            project_list=[]
            try:
                project_list=folder_data[str(fid)]
            except:
                pass
            record={
                "Name":name,
                "ID":fid,
                "Type":"Folder",
                "Projects":project_list,
                "Permissions":get_permissions("folder",fid),
                "SubFolders":read_folder(fid)
                }
            retval.append(clean_record(record))
        except:
            print("Error: {}".format(d))
    if str(id) == str(ORG):
        project_list=[]
        try:
            project_list=folder_data[str(id)]
        except:
            pass
        record = {
            "ID":str(id),
            "Type":"Organization",
            "Projects":project_list,
            "Permissions":get_permissions("org",str(id)),
            "SubFolders":retval
            }
        return clean_record(record)
    return retval

def get_projects():
    output = run_cmd("gcloud projects list --format=json")    
    flist = {}
    for d in json.loads(output):
        name = d["name"]
        projectId = d["projectId"]
        parent = d["parent"]["id"]
        record={
            "Name":str(name),
            "ProjectId":str(projectId),
            "Type":"Project",
            "Permissions": get_permissions("project",projectId)
            }
        try:
            flist[parent].append(record)
        except:
            flist[parent]=[]
            flist[parent].append(record)
    return flist

def get_permissions(id_type,id):
    output=""
    if id_type == "org":
        output = run_cmd("gcloud organizations get-iam-policy " + str(id) + " --format=json")
    if id_type == "folder":
        output = run_cmd("gcloud resource-manager folders get-iam-policy " + str(id) + " --format=json")
    if id_type == "project":
        output = run_cmd("gcloud projects get-iam-policy " + str(id) + " --format=json")
    plist={}
    try:
        for d in json.loads(output)["bindings"]:
            role=d["role"]
            members=d["members"]
            for m in members:
                try:
                    plist[m].append(role)
                except:
                    plist[m]=[]
                    plist[m].append(role)
    except:
        pass
    return plist

def re_search(val,arr):
    if re.match("^\w+:[\w\-]+@[\w\-\.]+$",val):
        if val in arr:
            return True
        else:
            return False
    else:
        for a in arr:
            if re.match(val,a):
                return True
    return False


def perm_search(data,search):
    found_perm = False
    perms = {}
    perm_keys = []
    try:
        perms = data.pop("Permissions")
        perm_keys = perms.keys()
    except: 
        pass
    folders = []
    if data["Type"] != "Project":
        try:
            folders = data.pop("SubFolders")
        except:
            pass
    try:
        projects = data.pop("Projects")
    except:
        projects = []
    data["Permissions"]={}
    if re_search(search,perm_keys):
        found_perm = True
        data["Permissions"][search]=perms[search]
    data["SubFolders"] = []
    if len(folders) > 0:
        for f in folders:
            sub = perm_search(f,search)
            if sub != None:
                data["SubFolders"].append(sub)
    data["Projects"] = []
    if len(projects) > 0:
        for p in projects:
            proj = perm_search(p,search) 
            if proj != None:
                data["Projects"].append(p)
    if found_perm:
        return clean_record(data)
    else:
        return None

def clean_search(val):
    m=re.match(r"^[\w\-]+@([\w\.\-]+)",val)
    if m:
        m=re.match(".*iam.gserviceaccount.com$",val)
        if m:
            return "serviceAccount:"+val
        else:
            return "user:"+val
    m=re.match(r"^[\w\.]+$",val)
    if m:
        return "domain:"+val
    return val

global folder_data

global args
args = parse_args()


data=[]

if args.org:
    global ORG
    ORG = args.org
    folder_data = get_projects()
    data = read_folder(ORG)
else:
    with open(args.input) as f:
        data=json.loads(f.read())

if args.find:
    search=clean_search(args.find)
    print(json.dumps(perm_search(data,search), indent=4))
else:
    print(json.dumps(data, indent=2))