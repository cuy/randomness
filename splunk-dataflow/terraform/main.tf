
locals {
    project_id = "splunk-export-${random_id.id.dec}"
    pubsub_topic = "splunk-dataflow-export"
}

provider "google" {
    project = local.project_id
}

##
## Project
##
resource "random_id" "id" {
    byte_length = 4
}

data "google_billing_account" "acct" {
    billing_account = var.billing_account
    open         = true
}

resource "google_project" "splunk_export_project" {
    name = var.splunk_export_project_name
    project_id = local.project_id
    org_id = var.organization
    billing_account = data.google_billing_account.acct.id
    # This is to address a race condition between the project/billing api and storage api
    provisioner "local-exec" {
        command = "sleep 10"
    }
}

resource "google_project_service" "stackdriver_service" {
    service = "stackdriver.googleapis.com"
    disable_dependent_services = true
    depends_on = [ google_project.splunk_export_project ]
}

resource "google_project_service" "dataflow_service" {
    service = "dataflow.googleapis.com"
    disable_dependent_services = true
    depends_on = [ google_project_service.stackdriver_service ]
}

##
## Pub/Sub Topic
##
resource "google_pubsub_topic" "export" {
    name = local.pubsub_topic
    depends_on = [ google_project.splunk_export_project]
}

resource "google_pubsub_topic_iam_binding" "pubsub_binding" {
    project = google_pubsub_topic.export.project
    topic = google_pubsub_topic.export.name
    role = "roles/pubsub.publisher"
    members = [google_logging_organization_sink.splunk_export.writer_identity]
}

resource "google_pubsub_subscription" "dataflow" {
    name = "dataflow"
    topic = google_pubsub_topic.export.name
    ack_deadline_seconds = 20
    retain_acked_messages = true
}

resource "google_pubsub_topic" "deadletter" {
    name = "${local.pubsub_topic}-deadletter"
    depends_on = [ google_project.splunk_export_project]
}

resource "google_pubsub_subscription" "deadletter_sub" {
    name = "deadletter"
    topic = google_pubsub_topic.deadletter.name
    ack_deadline_seconds = 20
    retain_acked_messages = false
}

##
## Logging Sink
##
resource "google_logging_organization_sink" "splunk_export" {
    name   = local.pubsub_topic
    org_id = var.organization
    destination = "pubsub.googleapis.com/projects/${local.project_id}/topics/${google_pubsub_topic.export.name}"
    filter = "resource.labels.project_id!=\"${local.project_id}\""
    include_children = true

}

## 
## GCS Bucket
##
resource "google_storage_bucket" "dataflow_bucket" {
    name     = google_project.splunk_export_project.project_id
    force_destroy = true
}

##
## Dataflow Template Deployment
##
resource "google_dataflow_job" "splunk_export" {
    name = var.dataflow_jobname
    template_gcs_path = "gs://dataflow-templates/latest/Cloud_PubSub_to_Splunk"
    temp_gcs_location = "gs://${google_storage_bucket.dataflow_bucket.name}/tmp"
    parameters = {
        inputSubscription = "projects/${local.project_id}/subscriptions/${google_pubsub_subscription.dataflow.name}"
        token = var.HEC_TOKEN
        url = var.HEC_URL
        outputDeadletterTopic = "projects/${local.project_id}/topics/${google_pubsub_topic.deadletter.name}",
        batchCount = var.BATCH_COUNT
        parallelism = var.PARALLELISM
        disableCertificateValidation = var.DISABLE_CERT_VALIDATION
    }
    machine_type = var.MACHINE_TYPE
    max_workers = var.MAX_WORKERS
    zone = var.zone
    on_delete = "cancel" # This shoudl be a drain but I use cancel for testing
    #on_delete = "drain"
    depends_on = [ google_project_service.dataflow_service ]
}

output project_id {
    value = google_project.splunk_export_project.project_id    
}