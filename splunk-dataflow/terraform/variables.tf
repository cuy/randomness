variable "organization" { 
    description = "Organization ID to monitor"
}
variable "splunk_export_project_name" { 
    description = "Project name to create that will house the dataflow pipeline and pub/sub export"
    default = "Splunk Export"
}

variable "billing_account" { 
    description = "Billing account ID to attach project to"
}

variable "zone" { 
    description = "Zone to deploy dataflow resources to"
}

variable "pubsub_topic" {
    description = "Topic name for Stackdriver to Pub/Sub export"
    default = "splunk-dataflow-export"
}

variable "dataflow_jobname" {
    description = "Job name for Dataflow"
    default = "splunk-export"
}

variable "HEC_TOKEN" {
    description = "Splunk HEC Token"
}

variable "HEC_URL" {
    description = "URL for HEC endpoint"
}

variable "BATCH_COUNT" {
    description = "Batch size for Dataflow job"
    default = 10
}

variable "PARALLELISM" {
    description = "Paralellism size for Dataflow job"
    default = 4
}

variable "DISABLE_CERT_VALIDATION" {
    description = "Whether or not to force validation of the HEC cert"
    default = false
}

variable "MACHINE_TYPE" {
    description = "Machine to use for Dataflow job"
    default = "n1-standard-4"
}

variable "MAX_WORKERS" {
    description = "Max number of workers in Dataflow"
    default = 15
}
