#!/bin/bash

# This script sets up the hipster shop.  For full details please see
# https://github.com/GoogleCloudPlatform/microservices-demo

# This requires skaffold - easiest to run this directly from cloud shell

# No need to edit past here.

# Get the project ID from gcloud
export PROJECT_ID=`gcloud config get-value project`

# Enable GKE and spin up a cluster
gcloud services enable container.googleapis.com
gcloud container clusters create demo --enable-autoupgrade \
    --enable-autoscaling --min-nodes=3 --max-nodes=10 --num-nodes=5 --zone=us-central1-a
kubectl get nodes

# Enable the container registry
gcloud services enable containerregistry.googleapis.com
gcloud auth configure-docker -q

# Clone hister shop repo
git clone https://github.com/GoogleCloudPlatform/microservices-demo
cd microservices-demo

## Use pre-built instead of compling
# Enable cloud build and deploy the hipster shop
# gcloud services enable cloudbuild.googleapis.com
#skaffold run -p gcb --default-repo=gcr.io/${PROJECT_ID}

kubectl apply -f ./release/kubernetes-manifests.yaml
sleep 30

external_ip=`kubectl get service frontend-external -o json | jq '.status.loadBalancer.ingress[].ip' | sed 's/"//g'`
echo "Hipster shop up and running at http://$external_ip"
