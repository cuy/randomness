#!/bin/bash

# This script sets up a simple GCP Stackdriver logging export to Splunk HEC
# for a single project.

# The project must be set up with gcloud config set project

# These variables need to be set
export HEC_URL=https://<HEC_ADDRESS>:8088/
export HEC_TOKEN=<HEC_TOKEN>

# No need to edit past here.

# Get the project ID from gcloud
export PROJECT_ID=`gcloud config get-value project`

# Enable the stackdriver monitoring api if not already used
gcloud services enable stackdriver.googleapis.com

# Create service account to be used by GCP Add-on for Splunk
export SERVICE_ACCOUNT=splunk-export-service-account
export SERVICE_ACCOUNT_FULL=${SERVICE_ACCOUNT}@${PROJECT_ID}.iam.gserviceaccount.com
gcloud iam service-accounts create $SERVICE_ACCOUNT
gcloud iam service-accounts keys create $SERVICE_ACCOUNT.json --iam-account="$SERVICE_ACCOUNT_FULL"
gcloud projects add-iam-policy-binding $PROJECT_ID --member="serviceAccount:$SERVICE_ACCOUNT_FULL" --role="roles/viewer"

# Create the pub/sub topics
export PUBSUB_EXPORT=splunk-dataflow-export
export DEADLETTER=splunk-dataflow-export-deadletter
export DATAFLOW_SUB=dataflow
gcloud pubsub topics create $PUBSUB_EXPORT
gcloud pubsub topics create $DEADLETTER
gcloud pubsub subscriptions create $DATAFLOW_SUB --topic $PUBSUB_EXPORT

# If using the TA to pull message instead of dataflow
#export SPLUNK_SUBSCRIPTION=splunk-pubsub-pull
#gcloud pubsub subscriptions create $SPLUNK_SUBSCRIPTION --topic $PUBSUB_EXPORT
#gcloud pubsub subscriptions add-iam-policy-binding $SPLUNK_SUBSCRIPTION \
# --member="serviceAccount:$SERVICE_ACCOUNT_FULL" \
# --role="roles/pubsub.subscriber"

# Create log sink
export SINK_NAME=splunk-sink
gcloud logging sinks create $SINK_NAME \
pubsub.googleapis.com/projects/$PROJECT_ID/topics/$PUBSUB_EXPORT \
 --log-filter="resource.type!=dataflow_step AND resource.type!=cloud_function"
export SERVICE_ACCOUNT=`gcloud logging sinks describe $SINK_NAME --format="value(writerIdentity)"`
gcloud pubsub topics add-iam-policy-binding $PUBSUB_EXPORT \
 --member="$SERVICE_ACCOUNT" --role="roles/pubsub.publisher"

# Create bucket for dataflow deployment
gsutil mb gs://${PROJECT_ID}-dataflow

# Enable Dataflow API and deploy template
gcloud services enable dataflow.googleapis.com

gcloud beta dataflow jobs run splunk-dataflow-`date +%s` \
--gcs-location=gs://dataflow-templates/latest/Cloud_PubSub_to_Splunk \
  --staging-location=gs://${PROJECT_ID}-dataflow/tmp \
  --max-workers=15 \
  --worker-machine-type=n1-standard-4 \
  --parameters="\
inputSubscription=projects/$PROJECT_ID/subscriptions/$DATAFLOW_SUB,\
token=$HEC_TOKEN,\
url=$HEC_URL,\
outputDeadletterTopic=projects/$PROJECT_ID/topics/$DEADLETTER,\
batchCount=10,\
parallelism=4,\
disableCertificateValidation=true"
