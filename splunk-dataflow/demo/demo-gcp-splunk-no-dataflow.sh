#!/bin/bash

# This script creates a single project log sink and generates a service account that can be added
# to the Splunk TA for GCP to pull logs.

# The project must be set up with gcloud config set project

# Set the project ID from gcloud
export PROJECT_ID=`gcloud config get-value project`

# Enable the api's if not already used
gcloud services enable stackdriver.googleapis.com
gcloud services enable pubsub.googleapis.com
gcloud services enable cloudscheduler.googleapis.com

# Create service account to be used by GCP Add-on for Splunk
export SERVICE_ACCOUNT=splunk-export-service-account
export SERVICE_ACCOUNT_FULL=${SERVICE_ACCOUNT}@${PROJECT_ID}.iam.gserviceaccount.com
gcloud iam service-accounts create $SERVICE_ACCOUNT
gcloud iam service-accounts keys create $SERVICE_ACCOUNT.json --iam-account="$SERVICE_ACCOUNT_FULL"
gcloud projects add-iam-policy-binding $PROJECT_ID --member="serviceAccount:$SERVICE_ACCOUNT_FULL" --role="roles/owner"

# Create the pub/sub topic and subscriptions
export PUBSUB_EXPORT=splunk-export
export SPLUNK_SUBSCRIPTION=splunk-ta
gcloud pubsub topics create $PUBSUB_EXPORT
gcloud pubsub subscriptions create $SPLUNK_SUBSCRIPTION --topic $PUBSUB_EXPORT
gcloud pubsub subscriptions add-iam-policy-binding $SPLUNK_SUBSCRIPTION \
 --member="serviceAccount:$SERVICE_ACCOUNT_FULL" \
 --role="roles/pubsub.subscriber"

# Create log sink - no exclusions so do not deploy dataflow/cloud function modules
export SINK_NAME=splunk-sink
gcloud logging sinks create $SINK_NAME \
pubsub.googleapis.com/projects/$PROJECT_ID/topics/$PUBSUB_EXPORT \
 --log-filter="resource.type!=dataflow_step AND resource.type!=cloud_function" --quiet
export SINK_ACCOUNT=`gcloud logging sinks describe $SINK_NAME --format="value(writerIdentity)"`
gcloud pubsub topics add-iam-policy-binding $PUBSUB_EXPORT \
 --member="$SINK_ACCOUNT" --role="roles/pubsub.publisher"

echo "Setup complete - Splunk TA for GCP credentials:"
echo " `pwd`/$SERVICE_ACCOUNT.json"