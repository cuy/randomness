#!/usr/bin/env python3

import sys
import json
import base64
from google.cloud import pubsub_v1
import argparse

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("error_file", help="JSON file to parse given the output of the Splunk Dataflow Export deadletter queue.  Command: gcloud pubsub subscriptions pull $deadletter_subscription [--limit=X] --format=json > error_file.json")
    parser.add_argument("--resend",nargs=2,metavar=("project_id","topic_name"),help="Republish events found in error_file.json to Pub/Sub Logging Sink 'topic_name' in 'project_id'.  The current serivce account assigned to GOOGLE_APPLICATION_CREDENTIALS must have permission to publish")
    parser.add_argument("--print_events",help="Output original pub/sub log entry",action='store_true')
    args = parser.parse_args()
    return args

def openfile(file):
    try:
        with open(sys.argv[1],'r') as f:
            data = f.read()
    except:
        print("Unable to open file")
        exit(1)
    return json.loads(data)

def parse_pubsub(msg):
    temp_data=base64.b64decode(msg).decode('utf-8')
    if "}{" in temp_data:
        return json.loads("[" + temp_data.replace("}{","},{") + "]")
    else: 
        return json.loads("[" + temp_data + "]")

def decode_message(msg):
    event = json.loads(message['event'])
    if args.print_events:
        print("**** Log Entry ***")
        print(json.dumps(event,indent=4))
    return event

def publish_event(publisher,topic_path,event):
    data=json.dumps(event).encode('utf-8')
    future = publisher.publish(topic_path, data=data)
    print(" Message {} submitted.".format(future.result()))

if __name__ == "__main__":
    global args
    args = parse_args()
    
    if args.resend:
        publisher = pubsub_v1.PublisherClient()
        topic_path = publisher.topic_path(args.resend[0], args.resend[1])

    error_file = openfile(args.error_file)

    i = 1
    for error in error_file:
        dataflow_error=error['message']['attributes']['errorMessage']
        dataflow_package=error['message']['data']
        pubsub_message_bundle = parse_pubsub(dataflow_package)
        if args.print_events:
            print("\n{}".format('='*80))
        print("Dataflow bundle [{}/{}]:\n Error: {}\n Pub/sub log messages contained: {}".format(i,len(error_file),dataflow_error,len(pubsub_message_bundle)))
        i = i + 1
        for message in pubsub_message_bundle:
            event = decode_message(message)
            if args.resend:
                publish_event(publisher,topic_path,event)
        print()
