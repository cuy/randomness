#!/bin/bash

# This requires jq (https://stedolan.github.io/jq/)
if [[ ! `which jq` ]]; then \
    echo "jq is required for this script (http://stedolab.github.io/jq/)"
    exit
fi

# Get the options from current running job
read RUNNING_JOB REGION <<< `gcloud dataflow jobs list --filter=running --format="value(JOB_ID,REGION)" 2> /dev/null`

if [[ $RUNNING_JOB == "" ]]; then \
    echo "Cannot find running dataflow job - ensure gcloud is configured for the correct project"
    exit
fi

ENV_OPT=`gcloud dataflow jobs show $RUNNING_JOB --environment --region=$REGION --format=json --format="value(environment.workerPools[0].metadata.sdk_pipeline_options)"`

# This sets all the variables to the existing values
STAGING_LOCATION=`echo $ENV_OPT | jq '.options.tempLocation' | sed -e 's/"//g'`
MAX_WORKERS=`echo $ENV_OPT | jq '.options.maxNumWorkers' | sed -e 's/"//g'`
MACHINE=`echo $ENV_OPT | jq '.options.machineType' | sed -e 's/"//g'`
HEC_TOKEN=`echo $ENV_OPT | jq '.options.token' | sed -e 's/"//g'`
HEC_URL=`echo $ENV_OPT | jq '.options.url' | sed -e 's/"//g'`
DEADLETTER=`echo $ENV_OPT | jq '.options.outputDeadletterTopic' | sed -e 's/"//g'`
BATCH_COUNT=`echo $ENV_OPT | jq '.options.batchCount' | sed -e 's/"//g'`
PARALLELISM=`echo $ENV_OPT | jq '.options.parallelism' | sed -e 's/"//g'`
DISABLE_CERT_VALIDATION=`echo $ENV_OPT | jq '.options.disableCertificateValidation' | sed -e 's/"//g'`
INPUT_SUB=`echo $ENV_OPT | jq '.options.inputSubscription' | sed -e 's/"//g'`

# Override any values here
MAX_WORKERS=25

CPU_LIMIT=`gcloud compute regions describe $REGION --format=json | jq '.quotas[] | select(.metric=="CPUS") | .limit'`
NEEDED_CORES=`echo "\`echo $MACHINE | cut -d- -f3\` * $MAX_WORKERS" | bc`

if [[ $NEEDED_CORES -gt $CPU_LIMIT ]]; then \
    echo "Need $NEEDED_CORES cores in project however current limit is $CPU_LIMIT"
    exit
fi

echo -n "Are you sure you wish to replace the dataflow job $RUNNING_JOB (yes to drain and redeploy): "
read validate
if [[ "$validate" == "YES" || "$validate" == "yes" ]];
then \
    # Drain existing job
    gcloud dataflow jobs drain $RUNNING_JOB

    # Submit new job
    gcloud beta dataflow jobs run splunk-dataflow-`date +%s` \
  --gcs-location=gs://dataflow-templates/latest/Cloud_PubSub_to_Splunk \
  --region=$REGION \
  --staging-location=$STAGING_LOCATION \
  --max-workers=$MAX_WORKERS \
  --worker-machine-type=$MACHINE \
  --parameters="\
inputSubscription=$INPUT_SUB,\
token=$HEC_TOKEN,\
url=$HEC_URL,\
outputDeadletterTopic=$DEADLETTER,\
batchCount=$BATCH_COUNT,\
parallelism=$PARALLELISM,\
disableCertificateValidation=$DISABLE_CERT_VALIDATION"
fi
