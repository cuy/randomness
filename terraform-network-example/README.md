# Shared VPC Network Example
## Summary
This is a sample Terraform that deploys an example of a master vm that is able to be reached by a a customer vm through a NAT interface.  This allows a customer to reach a shared service (like puppet, splunk endpoint, etc..) while not having all customer vm's on the master network.
## Architecture
![](https://gitlab.com/cuy/randomness/-/raw/master/terraform-network-example/Architecture.png)

## Requirements
- Terrafor, v0.12.20 only tested
- The master and customer projects need to be already created
- The service account the Terraform script runs as needs:
 - Compute Shared VPC Admin
 - Project Owner on both Master and Customer Projects

## Caveats & Questions
This repo is provided as is.
