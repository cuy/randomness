#############
## Compute ##
#############

resource "google_compute_instance" "master-vm" {
    name = "master-vm"
    machine_type = "n1-standard-2"
    zone = "${local.master.region}-${local.master.zone}"
    tags = ["master"]

    boot_disk {
    initialize_params {
        image = "ubuntu-os-cloud/ubuntu-1604-lts"
        type  = "pd-ssd"
        size  = "10"
    }
    }

    network_interface {
        network = google_compute_network.master_network.self_link
        subnetwork = local.master.subnet_name

        access_config {
            # Ephemeral IP
        }
    }

    metadata = {
      startup-script-url = "gs://cuyler-prototyping/scripts/run_puppet.sh"
    }
    depends_on = [google_compute_subnetwork.master_subnets]
}

#############
## Network ##
#############

##
## VPC
##
resource "google_compute_network" "master_network" {
  name = local.master.network_name
  auto_create_subnetworks = false
  routing_mode = "GLOBAL"
  project = google_compute_shared_vpc_host_project.host.project
}

resource "google_compute_subnetwork" "master_subnets" {
  count = length(var.master_regions)
  name = local.master.subnet_name
  ip_cidr_range = var.master_cidrs[var.master_regions[count.index]]
  region = var.master_regions[count.index]
  network = google_compute_network.master_network.self_link
}

resource "google_compute_shared_vpc_host_project" "host" {
  project = var.master_project
}

resource "google_compute_shared_vpc_service_project" "customer" {
  host_project    = google_compute_shared_vpc_host_project.host.project
  service_project = var.customer_project
}

##
## Firewall
##


resource "google_compute_firewall" "allow_master_ssh" {
    name    = "master-allow-ssh"
    network = google_compute_network.master_network.self_link
    target_tags = ["customer","master","nat"]
    allow {
        protocol = "tcp"
        ports    = ["22"]
    }
}

resource "google_compute_firewall" "allow_customer_ssh" {
  name    = "customer-allow-ssh"
  network = google_compute_network.master_network.self_link
    allow {
        protocol = "tcp"
        ports    = ["22"]
    }

   source_tags = ["customer"]
   target_tags = ["master"]
}

resource "google_compute_firewall" "master_allow_health_checks" {
  name    = "allow-health-checks-master"
  network = google_compute_network.master_network.self_link

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  source_ranges = ["35.191.0.0/16", "130.211.0.0/22"]
}

resource "google_compute_firewall" "master_allow_puppet" {
  name    = "allow-puppet-master"
  network = google_compute_network.master_network.self_link

  allow {
    protocol = "tcp"
    ports    = ["8140"]
  }
   source_tags = ["customer","nat","master"]
   target_tags = ["master"]
}

