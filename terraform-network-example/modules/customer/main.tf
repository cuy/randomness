provider "google" {
    project = var.project
}

provider "google-beta" {
    project = var.project
}

data "template_file" "config_nat_script" {
    template = file(format("${path.module}/config-nat.sh.tpl"))
    vars = {
        LOCAL_NETWORK = var.cidr
    }
}

#############
## Compute ##
#############

resource "google_compute_instance_template" "nat_template" {
    name_prefix             = "nat-template-${var.name}"
    machine_type            = "g1-small"
    tags                   = ["nat",var.name]
    region                  = var.region
  # boot disk
    disk {
        source_image = "ubuntu-os-cloud/ubuntu-1604-lts"
        disk_type    = "pd-ssd"
        disk_size_gb = "10"
    }
    network_interface { 
        network            = var.master_network
        subnetwork         = var.master_subnet
        subnetwork_project = var.master_project
        access_config { }
    }
    can_ip_forward = true
    # Secondary NIC on Customer Network
    network_interface {
        network            = google_compute_network.customer_network.self_link
        subnetwork         = google_compute_subnetwork.customer_subnet.self_link
        access_config { }
    }
    metadata = {
        startup-script     = data.template_file.config_nat_script.rendered
    }
    depends_on = [
        google_compute_subnetwork.customer_subnet
        ]
}

resource "google_compute_region_instance_group_manager" "nat_cluster" {
  provider           = google-beta
  name               = "nat-mig-${var.name}"
  region             = var.region
  base_instance_name = "nat-mig-${var.name}"

  target_size = var.nat_vm_count

  version {
    name              = "nat-mig-${var.name}-v0"
    instance_template = google_compute_instance_template.nat_template.self_link
  }

  depends_on = [
    google_compute_instance_template.nat_template
  ]
}

resource "google_compute_instance" "customer_vm" {
    name                   = "vm-${var.name}"
    machine_type           = "g1-small"
    zone                   = "${var.region}-${var.zone}"
    tags                   = ["customer",var.name]
    boot_disk {
        initialize_params {
            image          = "ubuntu-os-cloud/ubuntu-1604-lts"
            type           = "pd-ssd"
            size           = "10"
        }
    }
    # Primary NIC on Customer network
    network_interface {
        network            = google_compute_network.customer_network.self_link
        subnetwork         = google_compute_subnetwork.customer_subnet.self_link
        access_config { }
    }
    metadata = { }
    depends_on = [
        google_compute_subnetwork.customer_subnet
        ]
}

#############
## Network ##
#############

##
## VPC
##
resource "google_compute_network" "customer_network" {
    name                    = "cust-net-${var.name}"
    auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "customer_subnet" {
    name                   = "${var.name}-subnet"
    ip_cidr_range          = var.cidr
    region                 = var.region
    network                = google_compute_network.customer_network.self_link
}

##
## Route & Load Balancer
##
resource "google_compute_address" "nat_ilb_ip" {
    name                   = "nat-ilb-ip-${var.name}"
    address_type           = "INTERNAL"
    subnetwork             = google_compute_subnetwork.customer_subnet.self_link
    region                 = var.region
}

resource "google_compute_route" "nat" {
    count                  = length(var.master_cidrs)
    name                   = "nat-${var.name}-${count.index}"
    dest_range             = var.master_cidrs[var.master_regions[count.index]]
    network                = google_compute_network.customer_network.self_link
    next_hop_ilb           = google_compute_forwarding_rule.nat_fw.self_link
    # tags                   = ["customer"]
    priority               = 100
}

resource "google_compute_forwarding_rule" "nat_fw" {
    name                   = "nat-fwd-rule-${var.name}"
    region                 = var.region
    load_balancing_scheme  = "INTERNAL"
    ip_address             = google_compute_address.nat_ilb_ip.address
    backend_service        = google_compute_region_backend_service.nat_backend.self_link
    all_ports              = true
    network                = google_compute_network.customer_network.name
    subnetwork             = google_compute_subnetwork.customer_subnet.name
}

resource "google_compute_region_backend_service" "nat_backend" {
    provider               = google-beta
    name                   = "nat-lb-${var.name}"
    region                 = var.region
    health_checks          = [var.health_check]
    protocol               = "TCP"
    network                = google_compute_network.customer_network.self_link
    load_balancing_scheme  = "INTERNAL"
    backend {
        # group              = google_compute_instance_group.nat_group.self_link
        group              = google_compute_region_instance_group_manager.nat_cluster.instance_group
    }
}

##
## Firewall
##
resource "google_compute_firewall" "customer_allow_health_checks" {
    name                   = "allow-health-checks-${var.name}"
    network                = google_compute_network.customer_network.self_link

    allow {
        protocol = "tcp"
        ports    = ["22"]
    }
    target_tags            = [var.name]
    source_ranges          = ["35.191.0.0/16", "130.211.0.0/22"]
}

# Allow puppet to pass through the NAT
resource "google_compute_firewall" "customer_allow_puppet" {
    name                   = "allowpuppet-${var.name}"
    network                = google_compute_network.customer_network.self_link

    allow {
        protocol = "tcp"
        ports    = ["8140"]
    }
    target_tags            = ["nat"]
    source_tags            = [var.name]
}

# Opens up SSH for everything (just for demo)
resource "google_compute_firewall" "customer_allow_ssh" {
    name                   = "allow-ssh-${var.name}"
    network                = google_compute_network.customer_network.self_link

    allow {
        protocol = "tcp"
        ports    = ["22"]
    }
    target_tags            = [var.name]
    source_ranges          = ["0.0.0.0/0"]
}