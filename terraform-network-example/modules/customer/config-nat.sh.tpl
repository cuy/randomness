#!/bin/bash

# Need to sleep to ensure networks have come online
sleep 15

mgmt_nic=ens4
int_nic=ens5
int_ip=`curl -H 'Metadata-Flavor: Google' http://metadata.google.internal/computeMetadata/v1/instance/network-interfaces/1/ip`
int_gw=`curl -H 'Metadata-Flavor: Google' http://metadata.google.internal/computeMetadata/v1/instance/network-interfaces/1/gateway`
local_net=${LOCAL_NETWORK}
set -e
set -x

# Need to whitelist the health check IP's so they don't get blocked by sshguard
cat >> /etc/sshguard/whitelist << EOF
35.191.0.0/16
130.211.0.0/22
EOF

systemctl restart sshguard

sysctl -w net.ipv4.ip_forward=1
echo 1 > /proc/sys/net/ipv4/ip_forward
/sbin/iptables -w -t nat -A POSTROUTING -o $mgmt_nic -j MASQUERADE
echo "1 rt1" > /etc/iproute2/rt_tables
ip route add 35.191.0.0/16 via $int_gw dev $int_nic table rt1
ip route add 130.211.0.0/22 via $int_gw dev $int_nic table rt1
ip rule add from $local_net table rt1
