variable "project" { 
    description = "Project to deploy customer into (requires shared VPC to be set up)"
}
variable "name" { 
    description = "Customer name"
}
variable "region" { 
    description = "Region to deploy customer into"
}
variable "cidr" { 
    description = "CIDR range for customer subnet"
}
variable "zone" { 
    description = "Zone to deploy customer into"
}
variable "health_check" { 
    description = "Name of the health check for the ILB (gets decalred once per customer project so outside module)"
}
variable "nat_vm_count" { 
    default = 3
    type = number
}

variable "master_project" { 
    description = "Master project of the Shared VPC"
}
variable "master_network" { 
    description = "Master network of Shared VPC"
}
variable "master_subnet" { 
    description = "Master subnet of Shared VPC (easier if all subnets in VPC are named the same)"
}
variable "master_cidrs" { 
    description = "List of all master CIDRs to route to"
}
variable "master_regions" { 
    description = "List of all master regions to route to"
}

# This is just a work around for modules no able to have a depends_on 
# - need to ensure Master VPC is properly shared before VM's get created.
variable "wait_on" {
    default = {}

}