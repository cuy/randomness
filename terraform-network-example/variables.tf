
variable "master_project" {
  description = "Project for master deployment"
}

variable "customer_project" {
  description = "Project for customer deployment"
}

variable "master_cidrs" {
  type = map
  default = {
    us-east1 = "192.168.1.0/24"
    us-central1 = "192.168.2.0/24"
    us-west1 = "192.168.3.0/24"
  }
}

variable "master_regions" {
    type = list(string)
    default = ["us-east1", "us-central1", "us-west1"]
}

variable "customer_cidr" {
  default = "10.0.0.0/24"
}