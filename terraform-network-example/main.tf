# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

provider "google" {
    project = var.master_project
}

provider "google-beta" {
    project = var.master_project
}

locals {
    master = {
        name = "master-vm",
        region = "us-east1",
        zone = "c",
        network_name = "master-network"
        subnet_name = "master-subnet"
    }
}

###################
## Health Checks ##
###################
# Declare once in customer project
resource "google_compute_health_check" "nat_hc" {
    project = var.customer_project
    name                = "nat-hc"
    check_interval_sec  = 15
    timeout_sec         = 5
    healthy_threshold   = 2
    unhealthy_threshold = 3

    tcp_health_check {
        port = "22"
    }
}

###############
## Customers ##
###############

module "customer-eh" {
    source = "./modules/customer"
    
    project = var.customer_project
    name = "eh"
    region = "us-east1"
    cidr = var.customer_cidr
    zone = "b" # Zone of test VM
    master_project = var.master_project
    master_network = google_compute_network.master_network.self_link
    master_subnet = google_compute_subnetwork.master_subnets[length(var.master_regions)-1].name
    master_cidrs = var.master_cidrs
    master_regions = var.master_regions
    health_check = google_compute_health_check.nat_hc.self_link
    # This forces a module depends
    wait_on = google_compute_shared_vpc_service_project.customer
}

module "customer-bee" {
    source = "./modules/customer"
    
    project = var.customer_project
    name = "bee"
    region = "us-central1"
    cidr = var.customer_cidr
    zone = "b"
    master_project = var.master_project
    master_network = google_compute_network.master_network.self_link
    master_subnet = google_compute_subnetwork.master_subnets[length(var.master_regions)-1].name
    master_cidrs = var.master_cidrs
    master_regions = var.master_regions
    health_check = google_compute_health_check.nat_hc.self_link
    # This forces a module depends
    wait_on = google_compute_shared_vpc_service_project.customer
}

module "customer-sea" {
    source = "./modules/customer"
    
    project = var.customer_project
    name = "sea"
    region = "us-central1"
    cidr = var.customer_cidr
    zone = "c"
    master_project = var.master_project
    master_network = google_compute_network.master_network.self_link
    master_subnet = google_compute_subnetwork.master_subnets[length(var.master_regions)-1].name
    master_cidrs = var.master_cidrs
    master_regions = var.master_regions
    health_check = google_compute_health_check.nat_hc.self_link
    # This forces a module depends
    wait_on = google_compute_shared_vpc_service_project.customer
}